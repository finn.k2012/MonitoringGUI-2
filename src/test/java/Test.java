import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AClassWithOneUnitTest {

    @Test
    void demoTestMethod() {
        assertTrue(true);
    }

    @Test
    void test_JUit() {
    String str1 = "Test";
    assertEquals("Test",str1);
    }
    @Test
    void testPiAnzeige(){
      //arrange
     MonitoringPanel testMonitoring=  new MonitoringPanel()  ;
     String fakeCpuTemp = "50";
     String fakeCpuGhz = "0.6";

     //act
        testMonitoring.doFill(fakeCpuTemp,fakeCpuGhz,"","");

        //assert
        assertEquals(testMonitoring.getTextboxCPUTemp1().getText().toString(),fakeCpuTemp);
        assertEquals(testMonitoring.getTextboxCPUFrequenz1().getText().toString(),fakeCpuGhz);


    }
}


